#include "Scheduler.h"
#include "Led.h"
#include "TimeTracking.h"

#include <io.h>
#include <iomacros.h>

static void ExecuteTask(Taskp t) {
    /* ----------------------- INSERT CODE HERE ----------------------- */

    t->Invoked++;
    t->Taskf(t->ExecutionTime); // execute task


    /* ---------------------------------------------------------------- */

}

void Scheduler_P_FP(Task Tasks[]) {
    StartTracking(TT_SCHEDULER);

    uint8_t i;
    uint8_t active = NUMTASKS;

    for (i = 0; i < NUMTASKS; i++) {
        Taskp t = &Tasks[i];

        if (t->Flags & ACTIVE) {
            active = i;
            break;
        }
    }

   // P1OUT = i;

    uint8_t j;
    for (j = 0; j < active; j++) {

        Taskp tp = &Tasks[j];

        while (tp->Activated != tp->Invoked) {
            _EINT();
            tp->Flags |= ACTIVE;

            ExecuteTask(tp);

            tp->Flags &= ~ACTIVE;
            _DINT();
        }
    }
    StopTracking(TT_SCHEDULER);

}
